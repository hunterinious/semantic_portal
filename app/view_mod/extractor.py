from .models import View
from app.db import db
from app.test_mod.models import Thesis
from sqlalchemy import func
import typing as t


class ViewExtractor:

    def get_suitable_themes(self, descendants: list) -> t.List[str]:

        themes = list()
        # без distinct у концепта нет уникальности
        views_with_thesis_count = db.session.query(View.view, func.count(Thesis.thesis)).join(
            View, (Thesis.view == View.view)).filter(
            db.and_(Thesis.view.in_(descendants), Thesis.thesis != "")).group_by(View.view)

        for vt in views_with_thesis_count:
            if vt[1] >= 1:
                themes.append(vt[0])

        if not themes:
            raise ValueError('Not enough content in themes')

        return themes

    def get_all_themes(self) -> t.List[dict]:
        views_list = list()
        for index, view in enumerate(['css', 'html', 'Laravel —  PHP web framework', 'Flutter', 'Django']):
            views_dict = dict()
            views_dict["id"] = index
            views_dict["name"] = view
            views_dict["view"] = view
            views_list.append(views_dict)
        return views_list

    # def get_all_views(self):
    #     views = db.session.query(View.view).all()
    #     all_views = list()
    #     for v in views:
    #         all_views.append(v[0])
    #     return all_views

    def get_view_from_caption(self, caption: str) -> str:
        view = db.session.query(View.view).filter(View.caption_en == caption).distinct().all()
        view = view[0]
        db.session.close()
        return view

    def get_descendants(self, parent_view: str, descendants=list()) -> t.List[str]:
        children = db.session.query(View.view).filter(View.parent_view == parent_view)

        while children:
            descendants = descendants + children.all()

            children = db.session.query(View.view).filter(View.parent_view.in_(children))
            if len(children.all()) == 0:
                break
        db.session.close()
        return descendants

    def get_children(self, parent_view: str) -> View:
        if parent_view[0].find('php-') != -1:
            children = db.session.query(View).filter(View.parent_view.in_(parent_view))
        else:
            children = db.session.query(View).filter(View.parent_view.in_([parent_view]))
        db.session.close()
        return children
