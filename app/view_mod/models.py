from app.db import db


class View(db.Model):
    __tablename__ = 'views'
    id = db.Column(db.Integer, nullable=False, primary_key=True)
    view = db.Column(db.VARCHAR(100), nullable=False, default='')
    parent_view = db.Column('parentView', db.VARCHAR(100), default='default')
    caption_en = db.Column('captionEn', db.VARCHAR(255), default=None)
    intro_en = db.Column('introEn', db.VARCHAR(255), default=None)
    intro_escaped = False

    @property
    def is_escaped(self) -> str:
        return self.intro_escaped

    @is_escaped.setter
    def is_escaped(self, value: str):
        self.intro_escaped = value
