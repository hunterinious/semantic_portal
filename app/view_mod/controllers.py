from flask import jsonify, Blueprint
from .extractor import ViewExtractor

module = Blueprint('view', __name__, url_prefix='/view')


@module.route('/get_themes', methods=['GET'])
def get_themes():
    extractor = ViewExtractor()
    views = extractor.get_all_themes()
    return jsonify(views), 200
