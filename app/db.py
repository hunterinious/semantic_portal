from flask_sqlalchemy import SQLAlchemy
import redis

db = SQLAlchemy()
server = redis.StrictRedis(host="127.0.0.1", port=6379)
