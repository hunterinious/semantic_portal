import typing as t
from .models import Concept, Thesis


class ConceptStorage:
    def __init__(self, id: int, concept: str, domain: str, clas: str, forms: str, is_aspect: int, aspect_of: int,
                 is_escaped: bool):
        self.id = id
        self.concept = concept
        self.domain = domain
        self.clas = clas
        self.forms = forms
        self.is_aspect = is_aspect
        self.aspect_of = aspect_of
        self.is_escaped = is_escaped

    @classmethod
    def get_dump(cls, concepts: t.List[Concept]) -> t.List['ConceptStorage']:
        con_st_list = list()
        for c in concepts:
            concept_storage = cls(
                id=c.id,
                concept=c.concept,
                domain=c.domain,
                clas=c.clas,
                forms=c.forms,
                is_aspect=c.is_aspect,
                aspect_of=c.aspect_of,
                is_escaped=False
            )
            con_st_list.append(concept_storage)

        return con_st_list

    @property
    def name(self) -> str:
        return self.concept

    @name.setter
    def name(self, value: str):
        self.concept = value

    @property
    def get_id(self) -> int:
        return self.id


class ThesisStorage:
    def __init__(self, id: int, concept_id: int, view: str, thesis: str, clas: str, has_concept: bool, is_escaped: bool):
        self.id = id
        self.concept_id = concept_id
        self.view = view
        self.clas = clas
        self.thesis = thesis
        self.has_concept = has_concept
        self.is_escaped = is_escaped

    @classmethod
    def get_dump(cls, theses: t.List[Thesis]) -> t.List['ThesisStorage']:
        th_st_list = list()
        for th in theses:
            thesis_storage = cls(
                id=th.id,
                concept_id=th.concept_id,
                view=th.view,
                thesis=th.thesis,
                clas=th.clas,
                has_concept=th.has_concept,
                is_escaped=False
            )
            th_st_list.append(thesis_storage)

        return th_st_list

    @property
    def name(self) -> str:
        return self.thesis

    @name.setter
    def name(self, value: str):
        self.thesis = value

    @property
    def get_id(self) -> int:
        return self.concept_id

    @property
    def get_view(self) -> str:
        return self.view
