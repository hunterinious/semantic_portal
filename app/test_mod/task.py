from .template import Template
from .models import Concept, Thesis
from .entities_storage import ConceptStorage, ThesisStorage
from random import choice
from random import randint, sample
import typing as t


class Task:

    task_type = ""
    theme = ""
    template = dict()
    # aspects_ids = list()
    concepts_with_views = dict()
    main_with_aspects_and_vv = dict()
    used_entities_within_one_type = list()
    used_entities = dict()
    not_use_for_wa = list()
    question_model = Concept
    variant_model = Thesis

    def get_all_aspects(self, random_entities):
        aspects = [e for e in random_entities if e.is_aspect == 1]
        return aspects

    @classmethod
    def remove_abstract_aspects(cls, random_entities, aspects):
        aspects_of = [asp.aspect_of for asp in aspects]
        main_concepts = [e for e in random_entities if e.id in aspects_of]

        for m_concept in main_concepts:
            for asp in aspects:
                not_forms = False
                if m_concept.id == asp.aspect_of:
                    forms = m_concept.forms.split(";")
                    if not forms or forms == ['']:
                        not_forms = True
                    count = 0
                    for form in forms:
                        asp_lower = asp.concept.lower()
                        # if asp.concept == 'Variable parsing':
                        #     print(form.lower().strip(), m_concept.concept.lower(), "WRAPPER", asp.concept.lower())
                        if form.lower().strip() not in asp_lower and m_concept.concept.lower() not in asp_lower:
                            # print("here")
                            count += 1
                    if count == len(forms) or not_forms:
                        random_entities.remove(asp)
                        # aspects.remove(asp)
        return random_entities

    # @classmethod
    # def set_aspects_ids(cls, aspects):
    #     Task.aspects_ids = [e.id for e in aspects]

    def set_task_theme(self, ent: t.Union['ConceptStorage', 'ThesisStorage']):
        # set_theme = True

        # if Task.question_model == Concept:
        #     con_class = Task.views_with_classes.get(ent.clas)
        #     if con_class is None:
        #         set_theme = False
        #     elif con_class.get(view) is None:
        #         set_theme = False

        if Task.question_model == Thesis:
            Task.theme = ent.view
        else:
            views = Task.concepts_with_views[str(ent.id)]
            length = len(views)
            if length > 1:
                view_index = randint(0, len(views) - 1)
            elif length == 1:
                view_index = 0

            random_view = views[view_index]
            Task.theme = random_view

    def set_q_v_entites(self, concepts, theses, rand_template):
        if rand_template['questEntity'] == 'concept':
            q_entities = concepts
            v_entities = theses
        else:
            q_entities = theses
            v_entities = concepts
        return q_entities, v_entities

    def generate_task(self, concepts: t.Union[t.List[ConceptStorage], t.List[ThesisStorage]],
                      theses: t.Union[t.List[ConceptStorage], t.List[ThesisStorage]],
                      rand_template: dict) -> t.Tuple[t.Union[ConceptStorage, ThesisStorage],
                                                      t.Union[t.List[t.List[ConceptStorage]],
                                                              t.List[t.List[ThesisStorage]]]]:
        entity = Entity()

        q_entities, v_entities = self.set_q_v_entites(concepts, theses, rand_template)

        random_entity_questions = entity.get_random_entity_question(q_entities, rand_template)

        if len(random_entity_questions) > 0:
            random_entity_questions = sample(random_entity_questions, len(random_entity_questions))
            # theme_not_set = True
            variants = list()
            while random_entity_questions:
                rand_ent = choice(random_entity_questions)
                # if theme_not_set:
                self.set_task_theme(rand_ent)
                    # theme_not_set = False
                random_entity_questions.remove(rand_ent)
                variants = entity.get_suitable_variants(rand_ent, v_entities, rand_template)
                if variants:
                    # if Task.task_type == "matchItems":
                    #     Task.used_entities.append(rand_ent.name)
                    Task.template = rand_template
                    return rand_ent, variants

            if not variants:
                return self.change_template_and_regen(rand_template, q_entities, v_entities)
        else:
            return self.change_template_and_regen(rand_template, q_entities, v_entities)

    def change_template_and_regen(self, rand_template: dict,
                                  q_entities: t.Union[t.List[ConceptStorage], t.List[ThesisStorage]],
                                  v_entities: t.Union[t.List[ConceptStorage], t.List[ThesisStorage]]) -> t.Tuple[
                                  t.Union[ConceptStorage, ThesisStorage], t.Union[t.List[t.List[ConceptStorage]],
                                                                                  t.List[t.List[ThesisStorage]]]]:

        entity = Entity()
        template = Template()
        used_templates = list()
        temp_quest_entity = rand_template["questEntity"]
        # rand_temp = None
        # templates_amount = len(template.get_templates_by_entity(temp_quest_entity))
        # count = 0

        while True:

            rand_temp = template.get_random_type_template(Task.task_type, temp_quest_entity,
                                                          used_templates)
            if not rand_temp:
                used_templates = list()
                task_types_to_change = ("chooseOne", "chooseSeveral")
                Task.task_type = choice(task_types_to_change)

                if Task.question_model == Concept:
                    buf = q_entities
                    q_entities = v_entities
                    v_entities = buf
                    temp_quest_entity = 'thesis'
                    Task.question_model = Thesis
                    Task.variant_model = Concept
                else:
                    buf = q_entities
                    q_entities = v_entities
                    v_entities = buf
                    temp_quest_entity = 'concept'
                    Task.question_model = Concept
                    Task.variant_model = Thesis
                continue

            random_entity_questions = entity.get_random_entity_question(q_entities, rand_temp)
            random_entity_questions = sample(random_entity_questions, len(random_entity_questions))

            if not random_entity_questions:
                used_templates.append(rand_temp['id'])
                continue

            while random_entity_questions:
                rand_ent = choice(random_entity_questions)

                self.set_task_theme(rand_ent)

                random_entity_questions.remove(rand_ent)
                variants = entity.get_suitable_variants(rand_ent, v_entities, rand_temp)
                if variants:
                    Task.template = rand_temp
                    # if Task.task_type == "matchItems":
                    #     Task.used_entities.append(rand_ent.name)
                    return rand_ent, variants
                if len(random_entity_questions) == 0:
                    used_templates.append(rand_temp['id'])
            # count += 1

    def check_write_answer(self, answer: list, task_answer: str,
                           variants: dict, correct: int, wrong: int) -> t.Tuple[int, int]:
        answers_forms = answer[0]["answer"].split(";")

        if task_answer is not None:
            count = 0
            for a_forms in answers_forms:
                if task_answer.lower().strip() == a_forms.lower().strip():
                    count += 1
                    break

            if count == 1:
                correct += 1
            else:
                wrong += 1
            variants["user_answers"] = task_answer.lower().strip()
        else:
            wrong += 1

        # for index, ans_f in enumerate(answers_forms):
        #     answers_forms[index] = answers_forms[index].lower()
        variants["correct_answers"] = ";".join(answers_forms)

        return correct, wrong

    def check_gap_fill(self, answer: list, task_answer: str,
                       variants: dict, correct: int, wrong: int) -> t.Tuple[int, int]:
        answers_forms = answer[0]['answer'].split(";")

        if task_answer is not None:
            count = 0
            for a_forms in answers_forms:
                if task_answer[0].lower().strip() == a_forms.lower().strip():
                    count += 1
                    break

            if count == 1:
                correct += 1
            else:
                wrong += 1
            variants["user_answers"] = task_answer.lower().strip()
        else:
            wrong += 1
        #
        # if count == len(answer):
        #     correct += 1
        # else:
        #     wrong += 1
        # for index, ans_f in enumerate(answers_forms):
        #     answers_forms[index] = answers_forms[index].lower()
        variants["correct_answers"] = ";".join(answers_forms)

        return correct, wrong

    def check_match_items(self, answer: t.List[t.List[int]], task_answer: t.List[t.List[int]],
                          variants: dict, correct: int, wrong: int) -> t.Tuple[int, int]:
        if task_answer is not None:
            count = 0
            for index, ans in enumerate(answer):
                for ind, task_a in enumerate(task_answer):
                    if ans == task_a or ans == list(reversed(task_a)):
                        count += 1
                        break

            if count == len(answer):
                correct += 1
            else:
                wrong += 1
            variants["user_answers"] = task_answer
        else:
            wrong += 1

        variants["correct_answers"] = answer


        return correct, wrong

    def check_choose_one(self, answer: t.List[int], task_answer: int,
                         variants: dict, correct: int, wrong: int) -> t.Tuple[int, int]:
        if task_answer is not None:
            if task_answer == answer[0]["id"]:
                correct += 1
            else:
                wrong += 1
            variants["user_answers"] = task_answer
        else:
            wrong += 1

        variants["correct_answers"] = answer[0]["id"]

        return correct, wrong

    def check_several_or_remove(self, answer: t.List, task_answer: t.List[int],
                                variants: dict, correct: int, wrong: int) -> t.Tuple[int, int]:
        if task_answer is not None:
            count = 0
            for index, ans in enumerate(answer):
                for ind, task_a in enumerate(task_answer):
                    if task_a == ans["id"]:
                        count += 1
                        break

            # if count >= 1:
            #     correct += count / len(answer)
            if count == len(answer):
                correct += 1
            else:
                wrong += 1
            variants["user_answers"] = task_answer
        else:
            wrong += 1

        correct_answers = list()
        for ans in answer:
            correct_answers.append(ans["id"])

        variants["correct_answers"] = correct_answers

        return correct, wrong

    def check_task(self, task_type: str, answer: list, task_answer: t.Union[str, int, t.List[int], t.List[t.List[int]]],
                   variants: dict, correct: int, wrong: int) -> t.Tuple[int, int]:
        function = self.check_functions_variants.get(task_type)
        if function is None:
            raise ValueError(f'Invalid type, got {task_type}')
        else:
            cor, wr = function(self, answer, task_answer, variants, correct, wrong)

        return cor, wr

    check_functions_variants = {'writeAnswer': check_write_answer,
                                'gapFill': check_gap_fill,
                                'matchItems': check_match_items,
                                'chooseOne': check_choose_one,
                                'chooseSeveral': check_several_or_remove,
                                'removeWrong': check_several_or_remove}


class Entity:

    # abstract_ids = list()
    # check_for_contains = False

    def handle_mi_ent_quest(self, entities: t.List[ConceptStorage], template: dict) -> t.List[ConceptStorage]:
        # not_use = Task.used_entities.keys()
        used_one_type = Task.used_entities_within_one_type
        if not template['questClass']:
            random_entities = [e for e in entities if (e.name not in used_one_type and
                                                       e.clas not in template['questNotClass'])]
        else:
            random_entities = [e for e in entities if (e.name not in used_one_type and
                                                       e.clas not in template['questNotClass'] and
                                                       e.clas in template['questClass'])]
        return random_entities

    def handle_wa_ent_quest(self, entities: t.List[ThesisStorage], template: dict) -> t.List[ThesisStorage]:
        not_unique = 'essence-not-unic'
        not_use_for_wa = Task.not_use_for_wa
        # th_lists = Task.used_entities.values()
        # not_use = [th for th_list in th_lists for th in th_list]
        used_one_type = Task.used_entities_within_one_type

        if template['questClass'] == "":
            random_entities = [e for e in entities if (e.id not in not_use_for_wa and e.name not in used_one_type and
                                                       e.clas != not_unique and
                                                       e.clas not in template["questNotClass"])]
        else:
            random_entities = [e for e in entities if (e.id not in not_use_for_wa and e.name not in used_one_type and
                                                       e.clas != not_unique and
                                                       e.clas not in template["questNotClass"] and
                                                       e.clas in template['questClass'])]
        return random_entities

    def handle_other_ent_quest(self, entities: t.Union[t.List[ConceptStorage], t.List[ThesisStorage]],
                               template: dict) -> t.Union[t.List[ConceptStorage], t.List[ThesisStorage]]:
        if Task.question_model == Concept:
            not_unique = ''
            # not_use = list()
        else:
            not_unique = 'essence-not-unic'
            # th_lists = Task.used_entities.values()
            # not_use = [th for th_list in th_lists for th in th_list]
        used_one_type = Task.used_entities_within_one_type

        if not template['questClass']:
            random_entities = [e for e in entities if (e.clas != not_unique and e.name not in used_one_type and
                                                       e.clas not in template["questNotClass"])]
        else:
            random_entities = [e for e in entities if (e.clas != not_unique and e.name not in used_one_type and
                                                       e.clas not in template["questNotClass"] and
                                                       e.clas in template['questClass'])]
        return random_entities

    def get_random_entity_question(self, entities: t.Union[t.List[ConceptStorage], t.List[ThesisStorage]],
                                   template: dict) -> t.Union[t.List[ConceptStorage], t.List[ThesisStorage]]:

        function = self.handle_ent_quest_funcs.get(Task.task_type)
        if function is None:
            raise ValueError(f'Invalid type, got {type}')
        else:
            random_entities = function(self, entities, template)
        return random_entities

    def handle_right_vars_gf(self, q_entity: ConceptStorage, v_entities: t.List[ThesisStorage],
                             template: dict) -> t.List[ThesisStorage]:
        # not_to_use = list()
        quest_id = q_entity.get_id
        if not template['variantClass']:
            suitable_v_entities = [v for v in v_entities if (v.get_id == quest_id and
                                                             q_entity.name in v.name and
                                                             v.clas not in template["variantNotClass"])]
        else:
            suitable_v_entities = [v for v in v_entities if (v.get_id == quest_id and
                                                             q_entity.name in v.name and
                                                             v.clas not in template["variantNotClass"] and
                                                             v.clas in template["variantClass"])]
        return suitable_v_entities

    def handle_right_vars_mi(self, q_entity: ConceptStorage, v_entities: t.List[ThesisStorage],
                             template: dict) -> t.List[ThesisStorage]:
        # not_to_use = Task.used_entities.get(q_entity)
        quest_id = q_entity.get_id
        # if not_to_use is None:
        #     not_to_use = list()

        if not template['variantClass']:
            suitable_v_entities = [v for v in v_entities if (v.get_id == quest_id and
                                                             q_entity.name not in v.name and
                                                             v.clas not in template["variantNotClass"])]
        else:
            suitable_v_entities = [v for v in v_entities if (v.get_id == quest_id and
                                                             q_entity.name not in v.name and
                                                             v.clas not in template["variantNotClass"] and
                                                             v.clas in template["variantClass"])]
        return suitable_v_entities

    def handle_other_vars(self, q_entity: t.Union[ConceptStorage, ThesisStorage],
                          v_entities: t.Union[t.List[ConceptStorage], t.List[ThesisStorage]],
                          template: dict) -> t.Union[t.List[ConceptStorage], t.List[ThesisStorage]]:
        quest_id = q_entity.get_id
        # if Task.variant_model == Concept:
        #     not_to_use = list()
        # else:
        #     not_to_use = Task.used_entities.get(q_entity)
        #     if not_to_use is None:
        #         not_to_use = list()

        if not template['variantClass']:
            suitable_v_entities = [v for v in v_entities if (v.get_id == quest_id and
                                                             v.clas not in template["variantNotClass"])]
        else:
            suitable_v_entities = [v for v in v_entities if (v.get_id == quest_id and
                                                             v.clas not in template["variantNotClass"] and
                                                             v.clas in template["variantClass"])]
        return suitable_v_entities

    def get_right_variants(self, q_entity: t.Union[ConceptStorage, ThesisStorage],
                           v_entities: t.Union[t.List[ConceptStorage], t.List[ThesisStorage]],
                           template: dict) -> t.Union[t.List[ConceptStorage], t.List[ThesisStorage]]:
        function = self.handle_right_vars_funcs.get(Task.task_type)
        if function is None:
            raise ValueError(f'Invalid type, got {type}')
        else:
            suitable_v_entities = function(self, q_entity, v_entities, template)
        return suitable_v_entities

    def get_wrong_variants(self, q_entity: t.Union[ThesisStorage, ConceptStorage],
                           v_entities: t.Union[t.List[ConceptStorage], t.List[ThesisStorage]],
                           template: dict) -> t.Union[t.List[ConceptStorage], t.List[ThesisStorage]]:
        quest_id = q_entity.get_id
        not_unique = "essence-not-unic"

        if not template['variantClass']:
            suitable_v_entities = [v for v in v_entities if (v.get_id != quest_id and
                                                             self.main_and_aspects_not_in_one_task(v, q_entity) and
                                                             v.clas != not_unique and
                                                             v.clas not in template["variantNotClass"])]

        else:
            suitable_v_entities = [v for v in v_entities if (v.get_id != quest_id and
                                                             self.main_and_aspects_not_in_one_task(v, q_entity) and
                                                             v.clas != not_unique and
                                                             v.clas not in template["variantNotClass"] and
                                                             v.clas in template["variantClass"])]
        return suitable_v_entities

    def main_and_aspects_not_in_one_task(self, v,  q_entity):
        model = Task.question_model
        task_type = Task.task_type
        mwavv = Task.main_with_aspects_and_vv
        if model == Concept and task_type in ("chooseOne", "chooseSeveral", "removeWrong", "list-items"):
            is_aspect = mwavv[str(v.concept_id) + "r"]
            if q_entity.is_aspect:
                mains = mwavv[str(q_entity.id)]
                return not is_aspect and v.concept_id not in mains
            else:
                aspects = mwavv[str(q_entity.id)]
                return v.concept_id not in aspects
        elif model == Thesis and task_type == "chooseOne":
            is_aspect = mwavv[str(v.id) + "r"]
            if is_aspect:
                return False
            else:
                return q_entity.concept_id not in mwavv[str(v.id)]
        else:
            return True

    # def check_main_and_aspect_not_in_one_task(self, v, ids):
    #     model = Task.question_model
    #     task_type = Task.task_type
    #     if model == Concept and task_type in ("chooseOne", "chooseSeveral", "removeWrong", "list-items"):
    #         return v.concept_id not in ids
    #     elif model == Thesis and task_type == "chooseOne":
    #         return v.id not in ids
    #     else:
    #         return True

    # def get_aspects_or_mains_concepts(self, q_entity):
    #     model = Task.question_model
    #     task_type = Task.task_type
    #     concept = Concept()
    #     # print(Task.task_type)
    #     # print(model)
    #     if model == Concept and task_type in ("chooseOne", "chooseSeveral", "removeWrong", "list-items"):
    #         if q_entity.is_aspect:
    #             main_ids = concept.get_main_ids_by_ent_question(q_entity)
    #             # print("MAIN_IDS", len(main_ids))
    #             return main_ids
    #         else:
    #             aspects_ids = concept.get_aspects_ids_by_ent_question(q_entity)
    #             # print("ASPECTS_IDS", len(aspects_ids))
    #             return aspects_ids
    #     elif model == Thesis and task_type == "chooseOne":
    #         # print("TASK_ASPECTS_IDS", len(Task.aspects_ids))
    #         # aspects_ids = concept.get_aspects_of_thesis_owner(q_entity)
    #         # return aspects_ids
    #         return Task.aspects_ids
    #     else:
    #         return list()

    # def check_view(self, v_ent: t.Union[ConceptStorage, ThesisStorage]) -> bool:
    #     if Task.variant_model == Thesis:
    #         return v_ent.view == Task.theme
    #     else:
    #         return Task.theme in Task.concepts_with_views[str(v_ent.id)]

    def get_random_right_variants(self, right_vars: t.Union[t.List[ConceptStorage], t.List[ThesisStorage]],
                                  wrong_vars: t.Union[t.List[ConceptStorage], t.List[ThesisStorage]]) -> t.Union[
                                                                                            t.List[ConceptStorage],
                                                                                            t.List[ThesisStorage]]:
        task_type = Task.task_type
        right_vars_count = len(right_vars)
        wrong_vars_count = 0
        if task_type in ("chooseSeveral", "removeWrong"):
            wrong_vars_count = len(wrong_vars)

        number_of_true_answers = 0
        # В этом случае для генерации неправильных ответов(тез) нужно достать тезы такого же класа, но другого понятия
        if task_type in ("chooseSeveral", "removeWrong") or "list" in task_type:
            if right_vars_count < 5 < wrong_vars_count:
                if right_vars_count == 2:
                    number_of_true_answers = 2
                else:
                    number_of_true_answers = randint(2, right_vars_count)
            elif wrong_vars_count >= 5 and right_vars_count >= 5:
                number_of_true_answers = randint(2, 4)
            elif wrong_vars_count < 5 < right_vars_count:
                number_of_true_answers = randint(2, 4)
            elif wrong_vars_count <= 5 and right_vars_count <= 5:
                if wrong_vars_count > right_vars_count:
                    number_of_true_answers = 2
                elif wrong_vars_count < right_vars_count:
                    number_of_true_answers = randint(2, 4)
                elif right_vars_count == wrong_vars_count:
                    number_of_true_answers = randint(2, 4)
            random_right_variants = self.already_has(number_of_true_answers, right_vars)
        else:
            random_entity = choice(right_vars)
            random_right_variants = list()
            random_right_variants.append(random_entity)
        return random_right_variants

    def get_random_wrong_variants(self, right_vars: t.Union[t.List[ConceptStorage], t.List[ThesisStorage]],
                                  wrong_vars: t.Union[t.List[ConceptStorage], t.List[ThesisStorage]]) -> t.Union[
                                                                                            t.List[ConceptStorage],
                                                                                            t.List[ThesisStorage]]:
        length = len(right_vars)
        count_of_wrong_vars = 5 - length

        random_wrong_variants = self.already_has(count_of_wrong_vars, wrong_vars)

        return random_wrong_variants

    #  check if variant contains in selected variants for task
    def already_has(self, count: int,
                    variants: t.Union[t.List[ConceptStorage],
                                      t.List[ThesisStorage]]) -> t.Union[t.List[ConceptStorage], t.List[ThesisStorage]]:
        random_variants = list()
        already_has_count = 0
        while count != 0:
            random_wrong_var = choice(variants)
            check_for_already_has = True

            for wr_ent in random_variants:
                if random_wrong_var.name == wr_ent.name:
                    check_for_already_has = False
            if check_for_already_has or already_has_count == 35:
                random_variants.append(random_wrong_var)
                count -= 1
            already_has_count += 1
        return random_variants

    #  check if right and wrong variants contains question-entity
    def check_for_sub_str_in_all_entities(self, entity: t.Union[ConceptStorage, ThesisStorage],
                                          ri_ent: t.Union[t.List[ConceptStorage], t.List[ThesisStorage]],
                                          wr_ent: t.Union[t.List[ConceptStorage], t.List[ThesisStorage]]) -> bool:
        # task_type = Task.task_type
        name_lower = entity.name.lower()
        if Task.question_model == Concept:
            check = self.check_for_sub_str_concept_model(ri_ent, wr_ent, name_lower)
        else:
            check = self.check_for_sub_str_thesis_model(ri_ent, wr_ent, name_lower)
        return check

    def check_for_sub_str_concept_model(self, ri_ent, wr_ent, name_lower):
        check = True
        ri_len = len(ri_ent)
        wr_len = len(wr_ent)
        if (ri_len + wr_len) != 0:
            ri_count = 0
            if ri_len != 0:
                for ri_ent in ri_ent:
                    if name_lower in ri_ent.name.lower():
                        ri_count += 1
            wr_count = 0
            if wr_len != 0:
                for wr_ent in wr_ent:
                    if name_lower in wr_ent.name.lower():
                        wr_count += 1

            if ri_count >= 1 and wr_count == 0:
                check = False
        return check

    def check_for_sub_str_thesis_model(self, ri_ent, wr_ent, name_lower):
        check = True
        ri_len = len(ri_ent)
        wr_len = len(wr_ent)

        if (ri_len + wr_len) != 0:
            ri_count = 0
            if ri_len != 0:
                for ri_ent in ri_ent:
                    if ri_ent.name.lower() in name_lower:
                        ri_count += 1
            wr_count = 0
            if wr_len != 0:
                for wr_ent in wr_ent:
                    if wr_ent.name.lower() in name_lower:
                        wr_count += 1

            if ri_count >= 1 and wr_count == 0:
                check = False
        return check

    def get_suitable_variants(self, entity: t.Union[ConceptStorage, ThesisStorage],
                              v_entities: t.Union[t.List[ConceptStorage], t.List[ThesisStorage]], template: dict
                              ) -> t.Union[list, t.Tuple[t.List[ConceptStorage], t.List[ThesisStorage]],
                                           t.Tuple[t.Union[t.List[ConceptStorage], t.List[ThesisStorage]], list]]:
        task_type = Task.task_type
        # main_asp_ids = self.get_aspects_or_mains_concepts(entity)
        right_vars = self.get_right_variants(entity, v_entities, template)
        wrong_vars = list()
        if task_type not in ("writeAnswer", "matchItems", "gapFill"):
            wrong_vars = self.get_wrong_variants(entity, v_entities, template)
        length_right = len(right_vars)
        length_wrong = len(wrong_vars)

        if "choose" in task_type or task_type == "removeWrong":

            if (task_type == "chooseSeveral" and template['questEntity'] == 'concept') or task_type == "removeWrong":
                min_right_count = 2
                min_wrong_count = 3
            else:
                min_right_count = 1
                min_wrong_count = 4

            check = True
            if "choose" in task_type:
                check = self.check_for_sub_str_in_all_entities(
                    entity, right_vars, wrong_vars)

            if check and length_right >= min_right_count and length_wrong >= min_wrong_count:
                rand_right_vars = self.get_random_right_variants(right_vars, wrong_vars)
                rand_wrong_vars = self.get_random_wrong_variants(rand_right_vars, wrong_vars)
                return rand_right_vars, rand_wrong_vars
            else:
                return list()
        elif task_type in ("writeAnswer", "matchItems", "gapFill"):
            if len(right_vars) >= 1:
                rand_right_vars = self.get_random_right_variants(right_vars, wrong_vars)
                return rand_right_vars, list()
            else:
                return list()
        elif "list" in task_type:
            if length_right >= 5:
                min_wrong_count = 1
            else:
                min_wrong_count = 5 - length_right

            if length_right and length_wrong >= min_wrong_count:
                rand_right_vars = self.get_random_right_variants(right_vars, wrong_vars)
                rand_wrong_vars = self.get_random_wrong_variants(rand_right_vars, wrong_vars)
                return rand_right_vars, rand_wrong_vars
            else:
                return list()

    handle_ent_quest_funcs = {'writeAnswer': handle_wa_ent_quest,
                              'matchItems': handle_mi_ent_quest,
                              'gapFill': handle_other_ent_quest,
                              'chooseOne': handle_other_ent_quest,
                              'chooseSeveral': handle_other_ent_quest,
                              'removeWrong': handle_other_ent_quest,
                              'list-items': handle_other_ent_quest,
                              'list-sequence': handle_other_ent_quest}

    handle_right_vars_funcs = {'matchItems': handle_right_vars_mi,
                               'gapFill': handle_right_vars_gf,
                               'writeAnswer': handle_other_vars,
                               'chooseOne': handle_other_vars,
                               'chooseSeveral': handle_other_vars,
                               'removeWrong': handle_other_vars,
                               'list-items': handle_other_vars,
                               'list-sequence': handle_other_vars}
