import json
from random import randint
from app.enums import TaskType
from copy import deepcopy
import typing as ty
import os

class Template:

    templates = list()

    @classmethod
    def get_templates_with_views(cls):
        with open('templates_with_views.json', 'r', encoding='utf-8') as f:
            cls.templates = json.load(f)

    def get_templates(self):
        with open('templates.json') as f:
            self.templates = json.load(f)

    def get_suit_templates(self, themes):
        templates = deepcopy(Template.templates)
        for temp in templates:
            not_in_th_q_count, q_classes_amount = self.check_classes_in_views(temp, themes, "questClassViews")
            not_in_th_v_count, v_classes_amount = self.check_classes_in_views(temp, themes, "variantClassViews")
            if (not_in_th_q_count == len(themes) * q_classes_amount or
                    not_in_th_v_count == len(themes) * v_classes_amount):
                Template.templates.remove(temp)

    def check_classes_in_views(self, temp, themes, class_views):
        count = 0
        if temp.get(class_views):
            amount = len(temp[class_views].keys())
            for cl in temp[class_views].keys():
                for theme in themes:
                    if theme not in temp[class_views][cl]:
                        count += 1
        else:
            amount = 1
        return count, amount

    def get_possible_test_types(self):
        not_use_types = list()
        for type in TaskType:
            count = 0
            for temp in Template.templates:
                if type.value not in temp["types"]:
                    count += 1
            if count == len(Template.templates):
                not_use_types.append(type.value)
        return not_use_types

    def get_templates_by_entity(self, entity_type: str) -> ty.List[dict]:
        return [t for t in Template.templates if entity_type == t['questEntity']]

    def get_random_template(self, task_type: str) -> dict:
        random_templates = [t for t in Template.templates if task_type in t['types']]
        length = len(random_templates)
        if length > 1:
            random_template_index = randint(0, len(random_templates) - 1)
        elif length == 1:
            random_template_index = 0
        else:
            return dict()
        random_template = random_templates[random_template_index]
        return random_template

    # def get_template(self, id):
    #     return self.templates[id - 1]

    def get_random_type_template(self, task_type: str, entity_type: str, used_temp: ty.List[int]) -> dict:
        random_templates = [t for t in Template.templates if t['id'] not in used_temp and task_type in t['types'] and
                            t['questEntity'] == entity_type]
        length = len(random_templates)
        if length > 1:
            random_template_index = randint(0, len(random_templates) - 1)
        elif length == 1:
            random_template_index = 0
        else:
            return dict()
        random_template = random_templates[random_template_index]
        return random_template
