from werkzeug.utils import cached_property
from random import sample, choice
from app.view_mod.extractor import ViewExtractor
from app.view_mod.models import View
from .models import Thesis, Concept
from .task import Task, Entity
from app.enums import TaskType
from .template import Template
from copy import deepcopy
from app.test_mod.entities_storage import ConceptStorage, ThesisStorage
from operator import attrgetter
from app.db import db

import io
import json
import html
import re
import typing as ty


class Test:

    # used_entities = list()

    @cached_property
    def complexities_config(self) -> dict:
        with open('complexity.json') as f:
            complexity = json.load(f)

        return complexity

    def get_time(self, task_type: str) -> int:
        test_time = 0
        if task_type == "chooseOne":
            test_time = 30
        elif task_type == "chooseSeveral":
            test_time = 60
        elif task_type == "writeAnswer":
            test_time = 60
        elif task_type == "matchItems":
            test_time = 120
        elif task_type == "gapFill":
            test_time = 120
        elif task_type == "removeWrong":
            test_time = 180
        return test_time

    def division_variants_answers(self, tasks: ty.List[dict]) -> ty.Tuple[list, list]:
        task_list = list()
        answers_list = list()

        for t in tasks:
            task = dict()
            answers = dict()
            answers["id"] = t["id"]
            answers["answers"] = t["answers"]
            task["id"] = t["id"]
            task["type"] = t["type"]
            task["question"] = t["question"]
            task["variants"] = t["variants"]
            task["time"] = t["time"]
            task_list.append(task)
            answers_list.append(answers)

        return task_list, answers_list

    def set_tasks_amount(self, amount, not_use_types, complexity):
        types_of_tasks = {}
        count = 0
        for t_type in TaskType:
            value = t_type.value
            if complexity == "low":
                if value in ("matchItems", "gapFill", "removeWrong", "list-items"):
                    continue
            elif complexity == "medium":
                if value in ("gapFill", "removeWrong", "list-items", "list-sequence"):
                    continue

            if value not in not_use_types:
                types_of_tasks[value] = amount[count]
            else:
                if types_of_tasks.get("chooseOne"):
                    types_of_tasks["chooseOne"] += amount[count]
                else:
                    types_of_tasks["chooseOne"] = 1
            count += 1
        return types_of_tasks

    def get_tasks_by_percentage(self, complexity_conf: dict, descendants: ty.List[str]) -> ty.Dict[str, int]:
        template = Template()
        not_use_types = template.get_possible_test_types()

        # length = len(descendants)
        # tasks_amount = 10
        if complexity_conf["complexity"] == "low":
            each_task_amount = [5, 4, 1, 1]
            types_of_tasks = self.set_tasks_amount(each_task_amount, not_use_types, "low")
        elif complexity_conf["complexity"] == "medium":
            each_task_amount = [3, 3, 2, 2, 1, 1]
            types_of_tasks = self.set_tasks_amount(each_task_amount, not_use_types, "medium")
        elif complexity_conf["complexity"] == "hard":
            each_task_amount = [1, 2, 2, 1, 1, 2, 1, 1]
            types_of_tasks = self.set_tasks_amount(each_task_amount, not_use_types, "hard")

        return types_of_tasks

    def set_models(self, template: dict):
        if template['questEntity'] == 'concept':
            Task.question_model = Concept
            Task.variant_model = Thesis
        else:
            Task.question_model = Thesis
            Task.variant_model = Concept

    def get_entities_questions_count(self, template: dict) -> ty.Tuple[int, int]:
        if template['name'] == 'matchItems':
            start = 0
            end = 3
        else:
            start = 0
            end = 1

        return start, end

    def replacer(self, template: dict, r_entity: ty.Union[ConceptStorage, ThesisStorage]) -> str:
        name = r_entity.name
        question = template['question']
        if "###" in question:
            question = question.replace("###", "<i><b>" + name + "</b></i>")
        else:
            question += " <i><b>" + name + "</b></i>"
        return question

    def parse_entity_question(self, entity_question: ty.Union[ConceptStorage, ThesisStorage]):
        # ent_quest = deepcopy(entity_question)
        ent_quest = entity_question
        if Task.task_type not in ("removeWrong", "gapFill"):
            name_entity_in_question = ent_quest.name
            to_find = re.findall(r'\S+(?:jpg|jpeg|png)$', name_entity_in_question)
            if to_find != [] and "http" not in to_find[0] and to_find[0].find(".") != 0:
                source = "http://semantic-portal.net/images/" + name_entity_in_question
                ent_quest.name = "<img src=\"" + source + "\">"
            else:
                # random_entity_question.name = self.tag_parser(random_entity_question)
                if not ent_quest.is_escaped:
                    ent_quest.name = html.escape(ent_quest.name)
                    ent_quest.is_escaped = True
                # ent_quest.name = ent_quest.name.replace("<!--", "<!-").replace(
                #     "-->", "->").replace("<?php", "<span>").replace("?>", "</span>")
                if ent_quest.clas in ('democode', 'code', 'syntax'):
                    ent_quest.name = "<pre><code>%s</code></pre>" % ent_quest.name
        return ent_quest

    def parse_variants(self, variants: ty.Union[ty.List[ThesisStorage], ty.List[ConceptStorage]]):
        # vs = deepcopy(variants)
        vs = variants
        for v in vs:
            name_entity_in_variant = v.name
            to_find = re.findall(r'\S+(?:jpg|jpeg|png)$', name_entity_in_variant)
            if to_find != [] and "http" not in to_find[0] and to_find[0].find(".") != 0:
                source = "http://semantic-portal.net/images/" + name_entity_in_variant
                v.name = "<img src=\"" + source + "\">"
            else:
                # variants[index].name = self.tag_parser(variants[index])
                if not v.is_escaped:
                    v.name = html.escape(v.name)
                    v.is_escaped = True
                # v.name = v.name.replace("<!--", "<!-").replace(
                #     "-->", "->").replace("<?php", "<span>").replace("?>", "</span>")
                if v.clas in ('democode', 'code', 'syntax'):
                    v.name = "<pre><code>" + v.name + "</code></pre>"
        return vs

    def parse_question(self, random_template: dict, entity_question: ty.Union[ThesisStorage, ConceptStorage]) -> str:
        template = deepcopy(random_template)
        if Task.task_type not in ('matchItems', 'removeWrong', 'gapFill'):
            question = self.replacer(template, entity_question)
        else:
            question = template['question']

        if template["id"] in (1, 14, 15, 23) and Task.task_type == "chooseOne":
            question = question.replace("applicable", "the most applicable")

        return question

    def add_used_entities(self, entity_question: ty.Union[ThesisStorage, ConceptStorage],
                          variants: ty.Union[ty.List[ty.List[ThesisStorage]], ty.List[ty.List[ConceptStorage]]]):
        right_variants = variants[0]
        wrong_variants = variants[1]

        if Task.question_model == Thesis:
            concept = right_variants[0].name
        else:
            concept = entity_question.name
        if Task.used_entities.get(concept) is None:
            Task.used_entities[concept] = list()

        if Task.question_model == Thesis:
            thesis = entity_question.name
            Task.used_entities[concept].append(thesis)
        else:
            if Task.task_type == "removeWrong":
                vrs = wrong_variants
            else:
                vrs = right_variants
            for vi in vrs:
                thesis = vi.name
                Task.used_entities[concept].append(thesis)

    def add_used_entities_within_one_type(self, entity_question, variants):
        right_variants = variants[0]

        Task.used_entities_within_one_type.append(entity_question.name)
        for v in right_variants:
            Task.used_entities_within_one_type.append(v.name)


    def replace_hint(self, ent_question: ConceptStorage, variants: ty.List[ThesisStorage]):
        vs = deepcopy(variants)
        entity_question = deepcopy(ent_question)
        # wr_count = 0
        # for ent_wr_var in entities_wrong_variants:
        #     if random_entity_question.lower() in ent_wr_var.name.lower():
        #         count += 1
        # ri_count = 0
        # for ent_wr_var in entities_wrong_variants:
        #     if random_entity_question.lower() in ent_wr_var.name.lower():
        #         count += 1
        count = 0
        for v in vs:
            if entity_question.name.lower() in v.name.lower():
                count += 1

        if count > 1:
            # for index, v in enumerate(variants):
            #     words = v.name.split(" ")
            #     for word in words:
            #         if word.lower() in question.lower():
            #             stars = '*' * len(word)
            #             variants[index].name.replace(word, stars)
            for v in vs:
                rand_ent_quest_lower = entity_question.name.lower()
                v_lower = v.name.lower()
                stars = '*' * 8

                if rand_ent_quest_lower in v_lower:
                    v.name = v.name.replace(entity_question.name, stars)
                    v.name = v.name.replace(rand_ent_quest_lower, stars)

                if v.has_concept:
                    forms = entity_question.forms.split(";")
                    for form in forms:
                        if form == " " or form == "":
                            forms.remove(form)
                    for form in forms:
                        f_lower = form.lower()
                        if f_lower in v_lower:
                            v.name = v.name.replace(f_lower, stars)
                            v.name = v.name.replace(form, stars)
        return vs

    def replace_hint_MI(self, entity_questions_ac: ty.List[ConceptStorage], variants_ac: ty.List[ThesisStorage]):
        concept_thesis_dict = deepcopy(dict(zip(entity_questions_ac, variants_ac)))
        count = 0
        for key, value in concept_thesis_dict.items():
            if key.name.lower() in value.name.lower():
                count += 1
        if count > 1:
            for key, value in concept_thesis_dict.items():
                v_lower = value.name.lower()
                k_lower = key.name.lower()
                stars = '*' * 8

                if k_lower in v_lower:
                    value.name = value.name.replace(k_lower, stars)
                    value.name = value.name.replace(key.name, stars)

                if value.has_concept:
                    forms = key.forms.split(";")
                    for form in forms:
                        if form == " " or form == "":
                            forms.remove(form)
                    for form in forms:
                        f_lower = form.lower()
                        if f_lower in v_lower:
                            value.name = value.name.replace(f_lower, stars)
                            value.name = value.name.replace(form, stars)
        vs_ac = [value for value in concept_thesis_dict.values()]
        return vs_ac

    def build_task(self, variants_in_one: ty.Union[ty.List[ConceptStorage], ty.List[ThesisStorage]],
                   variants: ty.Union[ty.List[ty.List[ConceptStorage]],
                   ty.List[ty.List[ThesisStorage]]]) -> ty.Tuple[ty.List[ty.Dict], ty.List[ty.Dict]]:
        variants_list = list()
        answers_list = list()
        right_vars = variants[0]
        wrong_vars = variants[1]

        if Task.task_type == 'list-sequence':
            sorted(variants_in_one, key=attrgetter('name'))

        for index, v in enumerate(variants_in_one):
            variants_dict = dict()
            variants_dict["id"] = index
            if Task.task_type != "writeAnswer":
                variants_dict["answer"] = v.name

            if Task.task_type == "removeWrong":
                collection = wrong_vars
            else:
                collection = right_vars
            for ent in collection:
                if ent.id == v.id:
                    answers_dict = dict()
                    answers_dict["id"] = index
                    if Task.task_type == "writeAnswer" and Task.question_model == Thesis:
                        answers_dict["answer"] = v.name + ";" + v.forms
                    else:
                        answers_dict["answer"] = v.name
                    answers_list.append(answers_dict)
                    break
            variants_list.append(variants_dict)
        return answers_list, variants_list

    def build_task_GF(self, entity_question: ConceptStorage,
                      variants: ty.List[ThesisStorage]) -> ty.Tuple[ty.List[ty.Dict], ty.Dict]:
        vs = deepcopy(variants)
        # variants_list = list()
        answers_list = list()
        variants_dict = dict()
        for index, v in enumerate(vs):
            pattern = re.compile(re.escape(entity_question.name), flags=re.IGNORECASE)
            stars = '*' * len(entity_question.name)
            variants_dict = dict()
            variants_dict["id"] = index
            variants_dict["answer"] = pattern.sub(entity_question.name, v.name)
            # include_count = variants_dict["answer"].count(random_entity_question.concept)
            variants_dict["answer"] = variants_dict["answer"].replace(entity_question.name, stars, 1)

            # for inc in range(include_count):
            answers_dict = dict()
            answers_dict["id"] = index
            answers_dict["answer"] = entity_question.concept + ";" + entity_question.forms
            answers_list.append(answers_dict)
        return answers_list, variants_dict
        # variants_list.append(variants_dict)

    def build_task_MI(self, entity_questions_ac: ty.Union[ty.List[ThesisStorage], ty.List[ConceptStorage]],
                      variants_ac: ty.Union[ty.List[ThesisStorage], ty.List[ConceptStorage]]
                      ) -> ty.Tuple[ty.List[ty.List], ty.Dict[str, ty.List]]:
        variants_dict = dict()
        variants_dict["first"] = list()
        variants_dict["second"] = list()
        answers_list = list()

        index_variants = [0, 1, 2, 3, 4, 5]
        indexes_list = list()
        for _ in entity_questions_ac:
            indexes = list()
            first_index = choice(index_variants)
            index_variants.remove(first_index)
            second_index = choice(index_variants)
            index_variants.remove(second_index)
            indexes.append(first_index)
            indexes.append(second_index)
            indexes_list.append(indexes)
        answers_list = indexes_list
        collection = dict(zip(entity_questions_ac, variants_ac))
        for index, (key, value) in enumerate(collection.items()):
            first_dictionary = dict()
            second_dictionary = dict()
            first_dictionary["id"] = indexes_list[index][0]
            first_dictionary["answer"] = key.name
            first_dictionary["hint"] = "hint"
            second_dictionary["id"] = indexes_list[index][1]
            second_dictionary["answer"] = value.name
            second_dictionary["hint"] = "hint"
            variants_dict["first"].append(first_dictionary)
            variants_dict["second"].append(second_dictionary)
        variants_dict["second"] = sample(variants_dict["second"], len(variants_dict["second"]))
        return answers_list, variants_dict

    def build_task_info(self, right_variants, task_number):
        entity = right_variants[0]
        info_dict = dict()
        if isinstance(entity, ConceptStorage):
            th_view = db.session.query(Thesis.view).join(Concept, (Thesis.concept_id == entity.id)).first()[0]
            view_info = db.session.query(View).filter(View.view == th_view).first()
        else:
            view_info = db.session.query(View).filter(View.view == entity.view).first()

        intro = view_info.intro_en
        if not view_info.is_escaped:
            intro = html.escape(intro)
            view_info.is_escaped = True

        info_dict["id"] = task_number
        info_dict["view"] = view_info.view
        info_dict["caption_en"] = view_info.caption_en
        info_dict["intro_en"] = intro

        return info_dict

    def themes_to_learn(self, to_learn):
        to_learn = list({v['url']: v for v in to_learn}.values())
        return to_learn

    def pack_task(self, answers: ty.Union[ty.Dict, ty.List], variants: ty.List, question: str,
                  task_number: int, time: int) -> dict:
        task = dict()
        task["id"] = task_number
        task["type"] = Task.task_type
        task["question"] = question
        task["variants"] = variants
        task["answers"] = answers
        task["time"] = time

        return task

    def append_to_accs(self, question, variants, quest_ac, variants_ac):
        quest_ac.append(question)
        for v in variants:
            variants_ac.append(v)

    def generate_test(self, parent_view: str, complexity_level: int = 0) -> ty.List[dict]:
        if complexity_level not in (0, 1, 2):
            raise ValueError(f'Invalid complexity value, got {complexity_level}')

        view_extractor = ViewExtractor()
        concept = Concept()
        thesis = Thesis()
        template = Template()
        task = Task()

        descendants = view_extractor.get_descendants(parent_view)
        if not descendants:
            raise ValueError('Empty Descendants')

        complexity_conf = self.complexities_config[complexity_level]

        descendants_list = list()
        for d in descendants:
            descendants_list.append(d[0])

        themes = view_extractor.get_suitable_themes(descendants_list)
        themes = sample(themes, len(themes))
        Template.get_templates_with_views()
        template.get_suit_templates(themes)

        type_of_tasks = self.get_tasks_by_percentage(complexity_conf, descendants)

        concepts = concept.get_concepts(themes)
        concepts = ConceptStorage.get_dump(concepts)
        aspects = task.get_all_aspects(concepts)
        concepts = Task.remove_abstract_aspects(concepts, aspects)
        # Task.set_aspects_ids(aspects)

        theses = thesis.get_theses(themes)
        theses = ThesisStorage.get_dump(theses)
        Task.not_use_for_wa = thesis.theses_not_for_wa()

        with io.open('concepts_with_views.json') as f:
            Task.concepts_with_views = json.load(f)
        with io.open('main_with_aspects_and_vv.json') as f:
            Task.main_with_aspects_and_vv = json.load(f)

        generated_tasks = list()
        info_list = list()
        task_number = 0
        for task_type, count in type_of_tasks.items():
            Task.used_entities_within_one_type = list()
            Task.task_type = task_type
            for c in range(count):
                random_template = template.get_random_template(task_type)
                if random_template['name'] in ('matchItems', 'gapFill', 'removeWrong', 'list-items', 'list-sequence'):
                    random_template['questEntity'] = 'concept'
                self.set_models(random_template)
                start, end = self.get_entities_questions_count(random_template)

                entity_questions_ac = list()
                variants_ac = list()

                for j in range(start, end):
                    entity_question, variants = task.generate_task(concepts, theses, random_template)
                    random_template = Task.template
                    # self.add_used_entities(entity_question, variants)
                    self.add_used_entities_within_one_type(entity_question, variants)

                    right_variants = variants[0]
                    wrong_variants = variants[1]
                    variants_in_one_list = right_variants + wrong_variants

                    entity_question = self.parse_entity_question(entity_question)
                    if Task.task_type != "gapFill":
                        variants_in_one_list = self.parse_variants(variants_in_one_list)
                    question = self.parse_question(random_template, entity_question)

                    if Task.task_type == "matchItems":
                        self.append_to_accs(entity_question, variants_in_one_list, entity_questions_ac, variants_ac)

                task_time = self.get_time(Task.task_type)

                if Task.task_type in ("chooseOne", "chooseSeveral", "removeWrong"):
                    variants_in_one_list = sample(variants_in_one_list, len(variants_in_one_list))
                if Task.task_type in ("chooseOne", "chooseSeveral") and Task.question_model == Concept:
                    variants_in_one_list = self.replace_hint(entity_question, variants_in_one_list)
                elif c == 1 and Task.task_type == "matchItems":
                    variants_ac = self.replace_hint_MI(entity_questions_ac, variants_ac)

                if Task.task_type not in ("matchItems", "gapFill"):
                    answers_list, variants_list = self.build_task(variants_in_one_list, variants)
                elif Task.task_type == "gapFill":
                    answers_list, variants_dict = self.build_task_GF(entity_question, variants_in_one_list)
                else:
                    answers_list, variants_dict = self.build_task_MI(entity_questions_ac, variants_ac)

                if Task.task_type not in ("matchItems", "gapFill"):
                    generated_task = self.pack_task(answers_list, variants_list, question, task_number, task_time)
                else:
                    generated_task = self.pack_task(answers_list, variants_dict, question, task_number, task_time)

                info_dict = self.build_task_info(right_variants, task_number)
                info_list.append(info_dict)
                generated_tasks.append(generated_task)
                task_number += 1
        return generated_tasks, info_list
