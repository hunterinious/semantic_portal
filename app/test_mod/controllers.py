from flask import jsonify, request, Blueprint, session
import json
import redis
from app.view_mod.extractor import ViewExtractor
from app.test_mod.test import Test
from app.test_mod.task import Task
from app.user_mod.models import UserTest
from sqlalchemy import exc
from functools import lru_cache
import random
from uuid import uuid4
from time import time
from app.db import server, db
from copy import deepcopy
from loguru import logger

logger.add("app/logs/log.log")
module = Blueprint('test', __name__, url_prefix='/test')


@lru_cache(maxsize=None)
def generate_test(theme, complexity_level, seed):
    random.seed(seed)
    test = Test().generate_test(theme, complexity_level)
    return test


@module.route('/setup_test', methods=['GET'])
def setup_test():
    try:
        view = ViewExtractor()
        views = view.get_all_themes()
        views_list = list()
        for v in views:
            views_list.append(v['name'])
        views_array = random.sample(views_list, len(views_list))

        complexity_level = int(request.args.get('complexity_level', 2))
        seed = int(request.args.get('seed', time()))
        caption = request.args.get('theme', random.choice(views_array))
        theme = view.get_view_from_caption(caption)

        try:
            tasks, info_list = generate_test(theme, complexity_level, seed)
        except ValueError as e:
            logger.exception('got exception on generate test', exc_info=True)
            return jsonify(error=str(e), description="Test not generated"), 500
        except exc.OperationalError as e:
            logger.exception('got exception on generate test', exc_info=True)
            return jsonify(error=str(e), description="DB Error"), 500

        test = Test()
        task_dicts, answers_dicts = test.division_variants_answers(tasks)

        uuid = str(uuid4())[:16]
        uuid_list = server.get('uuid_list')
        if uuid_list is None:
            uuid_list = list()
        else:
            uuid_list = json.loads(uuid_list)
        while uuid in uuid_list:
            uuid = str(uuid4())[:16]
        uuid_list.append(uuid)

        try:
            server.set('uuid_list', json.dumps(uuid_list))
            server.set(f'{uuid}_info', json.dumps(info_list))
            server.set(f'{uuid}_to_learn', json.dumps(list()))
            server.set(f'{uuid}_test', json.dumps(task_dicts))
            server.set(f'{uuid}_answer', json.dumps(answers_dicts))
        except redis.exceptions.DataError as e:
            return jsonify(eror=str(e), description="Can not convert data")
        server.set(f'{uuid}_correct_answers_count', 0)
        server.set(f'{uuid}_wrong_answers_count', 0)

        user_test = UserTest.create_user_test(caption, complexity_level, session['username'], session['group'])
        db_id = user_test.id
        session['db_test_id'] = db_id
    except Exception as e:
        logger.exception('got exception setup_test', exc_info=True)
        return jsonify(error=str(e), description="setup test Error"), 500

    return jsonify({'test_id': uuid, 'amount': len(task_dicts)}), 200


@module.route('/next_task/<uuid>', methods=['GET'])
def next_task(uuid):
    try:
        test = server.get(f'{uuid}_test')
        test = json.loads(test)

        if test is None:
            return jsonify(error='Test with specified uuid not found', uuid=uuid), 400

        if not test:
            return jsonify("All done")

        task = test[0]
        task_type = test[0]["type"]
        # del test[0]

        try:
            # server.set(f'{uuid}_test', json.dumps(test))
            server.set(f'{uuid}_type', json.dumps(task_type))
        except redis.exceptions.DataError as e:
            return jsonify(eror=str(e), description="Can not convert data"), 500
    except Exception as e:
        logger.exception('Got exception next task', exc_info=True)
        return jsonify(error=str(e), description="next task Error"), 500

    return jsonify(task)


@module.route('/check_task/<uuid>', methods=['POST'])
def check_task(uuid):
    try:
        answers = server.get(f'{uuid}_answer')
        variants_server = json.loads(server.get(f'{uuid}_test'))
        type = server.get(f'{uuid}_type')
        correct_count = int(server.get(f'{uuid}_correct_answers_count'))
        wrong_count = int(server.get(f'{uuid}_wrong_answers_count'))
    except TypeError as e:
        return jsonify(eror=str(e), description="key was not defined"), 500

    task_answer = request.get_json()

    correct = correct_count
    wrong = wrong_count
    answers = json.loads(answers)
    variants = deepcopy(variants_server[0])
    del variants_server[0]
    type = json.loads(type)

    try:
        answer = answers[0]
    except ValueError as e:
        return jsonify(error=str(e), description="Answers of test are empty"), 500
    answer = answer["answers"]
    del answers[0]

    try:
        task_answer = task_answer["task_answers"]
    except KeyError as e:
        return jsonify(eror=str(e), description="Empty user test answers"), 400

    task = Task()
    try:
        correct, wrong = task.check_task(type, answer, task_answer, variants, correct, wrong)
    except ValueError as e:
        return jsonify(eror=str(e), description="Wrong task type"), 400

    if correct_count == correct and wrong_count == wrong:
        return jsonify("not checked", task_answer, answer)
    else:
        if wrong > wrong_count:
            info_list = json.loads(server.get(f'{uuid}_info'))
            to_learn_list = json.loads(server.get(f'{uuid}_to_learn'))

            to_learn = info_list[variants["id"]]

            to_learn_dict = dict()
            to_learn_dict["id"] = len(to_learn_list)
            to_learn_dict["url"] = "https://semantic-portal.net/" + to_learn["view"]
            to_learn_dict["title"] = to_learn["caption_en"]
            to_learn_dict["intro"] = to_learn["intro_en"]
            to_learn_list.append(to_learn_dict)

            server.set(f'{uuid}_to_learn', json.dumps(to_learn_list))
        try:
            server.set(f'{uuid}_correct_answers_count', correct)
            server.set(f'{uuid}_wrong_answers_count', wrong)
            server.set(f'{uuid}_answer', json.dumps(answers))
            server.set(f'{uuid}_test', json.dumps(variants_server))
        except redis.exceptions.DataError as e:
            return jsonify(eror=str(e), description="Can not convert data"), 500
        return jsonify(variants), 200
