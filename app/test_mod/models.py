from app.db import db
import typing as t


class Concept(db.Model):
    __tablename__ = 'concepts'
    id = db.Column(db.Integer, primary_key=True)
    concept = db.Column(db.VARCHAR(255), nullable=False, default='')
    domain = db.Column(db.VARCHAR(100), nullable=False, default='')
    clas = db.Column('class', db.VARCHAR(100), nullable=False, default='')
    forms = db.Column(db.VARCHAR(255), nullable=True, default=None)
    is_aspect = db.Column('isAspect', db.Integer, nullable=False, default='0')
    aspect_of = db.Column('aspectOf', db.Integer, default=None)

    def get_concepts(cls, themes: t.List[str]) -> t.List['Concept']:
        concepts = db.session.query(Concept).join(Thesis, (Thesis.concept_id == Concept.id)).filter(
            db.and_(Thesis.thesis != "", Concept.concept != "", Thesis.view.in_(themes)))
        if concepts.count() == 0:
            raise ValueError('Concepts not found at this theme')
        db.session.close()
        return concepts.all()

    def get_all_concepts_classes(self):
        classes = db.session.query(Concept.clas).distinct().all()
        return classes

    # def get_main_ids_by_ent_question(self, concept, mains_ids=list()):
    #     main_id = db.session.query(Concept.aspect_of).filter(Concept.id == concept.id)
    #
    #     while main_id:
    #         mains_ids.append(main_id.first()[0])
    #         main_id = db.session.query(Concept.aspect_of).filter(Concept.is_aspect == 1,
    #                                                              Concept.id == mains_ids[len(mains_ids) - 1])
    #         if len(main_id.all()) == 0:
    #             break
    #     db.session.close()
    #     return mains_ids
    #
    # def get_aspects_ids_by_ent_question(self, concept, aspects_ids=list()):
    #     aspect_id = db.session.query(Concept.id).filter(Concept.aspect_of == concept.id)
    #     if aspect_id.all() is None:
    #         return list()
    #     while aspect_id:
    #         aspects_ids = aspects_ids + aspect_id.all()
    #         aspect_id = db.session.query(Concept.id).filter(Concept.aspect_of in aspects_ids)
    #
    #         if len(aspect_id.all()) == 0:
    #             break
    #     db.session.close()
    #     return aspects_ids

    # def get_aspects_of_thesis_owner(self, thesis):


# Нужно ли данные, которые я вытащил из БД устанавливать в поля экземпляров
# Может возникунть ситуация, когда тезы в разных текстах(неправильных) могут повторяться
class Thesis(db.Model):
    __tablename__ = 'thesises'
    id = db.Column(db.Integer, primary_key=True)
    concept_id = db.Column(db.Integer, nullable=False, default='0')
    view = db.Column(db.VARCHAR(100), nullable=False, default='')
    thesis = db.Column(db.Text, nullable=False)
    clas = db.Column('class', db.VARCHAR(100), nullable=False, default='')
    has_concept = db.Column('hasConcept', db.Integer, nullable=False, default=0)

    def get_theses(self, themes: t.List[str]) -> t.List['Thesis']:
        sub_query = db.session.query(TClass.name).filter(TClass.visible == 1)

        theses = db.session.query(Thesis).join(Concept, (Thesis.concept_id == Concept.id)).filter(
            db.and_(Concept.concept != "", Thesis.thesis != "", Thesis.view.in_(themes)),
            Thesis.clas.in_(sub_query))

        if theses.count() == 0:
            raise ValueError('Theses not found at this theme')
        db.session.close()

        return theses.all()

    def get_all_theses_classes(self):
        classes = db.session.query(Thesis.clas).distinct().all()
        return classes

    def theses_not_for_wa(self) -> t.List[int]:
        th_not_for_wa = db.session.query(Thesis.id).join(Concept, (Thesis.concept_id == Concept.id)).filter(
            db.and_(Concept.concept != "", Thesis.thesis != "", Thesis.has_concept)).distinct().all()
        db.session.close()

        th_not_for_wa = [th for th_list in th_not_for_wa for th in th_list]

        return th_not_for_wa


class TClass(db.Model):
    __tablename__ = 'tClasses'
    name = db.Column(db.VARCHAR(100), primary_key=True)
    id = db.Column(db.Integer, nullable=False, unique=True)
    visible = db.Column(db.Integer, nullable=False, default=1)
