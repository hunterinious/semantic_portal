from flask import jsonify, request, Blueprint, session
from app.db import db, server
from .models import UserTest
from ..test_mod.test import Test
from loguru import logger
import json
import redis

module = Blueprint('user', __name__, url_prefix='/user')


@module.route('/identification',  methods=['POST', 'GET'])
def identification():
    req_j = request.get_json()
    try:
        user_title = req_j['title']
        user_group = req_j['group']
    except KeyError as e:
        return jsonify(error=str(e), description="key was not specified"), 400

    session['username'] = user_title
    session['group'] = user_group

    return jsonify("User has been successfully identified"), 200


@module.route('/get_result/<uuid>', methods=['POST'])
def get_result(uuid):
    test = Test()
    # url, title, intro
    result = dict()
    to_learn_list = json.loads(server.get(f'{uuid}_to_learn'))
    to_learn_list = test.themes_to_learn(to_learn_list)
    try:
        correct = int(server.get(f'{uuid}_correct_answers_count'))
        wrong = int(server.get(f'{uuid}_wrong_answers_count'))
    except TypeError as e:
        return jsonify(eror=str(e), description="Count of correct and wrong answers were not defined"), 500

    user_test_id = session['db_test_id']

    result["correct_answers"] = correct
    result["wrong_answers"] = wrong

    uuid_list = json.loads(server.get('uuid_list'))
    try:
        uuid_list.remove(uuid)
    except ValueError as e:
        return jsonify(error=str(e), description="You try again to get result"), 400

    try:
        server.set('uuid_list', json.dumps(uuid_list))
    except redis.exceptions.DataError as e:
        return jsonify(eror=str(e), description="Can not convert data"), 500

    if correct + wrong == 0:
        UserTest.end_user_test(user_test_id, {"mark": 0, "percent": 0})
        return jsonify("Every test not checked")

    percents = (correct * 100) / (correct + wrong)
    UserTest.get_test_result(result, percents)
    UserTest.end_user_test(user_test_id, result)

    return jsonify({"result": result, "to_learn": to_learn_list}), 200


@module.route('/get_info', methods=['GET'])
def get_info():
    try:
        user_tests = db.session.query(UserTest).filter(UserTest.level != None, UserTest.theme != '',
                                                       UserTest.mark != None, UserTest.start != None,
                                                       UserTest.end !=  None)
    except Exception as e:
        logger.exception('got exception on generate test', exc_info=True)
        return jsonify(error=str(e), description="DB Error"), 500

    count = user_tests.count()

    if count == 0:
        return jsonify({'info': []})

    user_tests_list = list()
    for ut in user_tests.all():
        level = ut.level
        if level == 0:
            level = "low"
        elif level == 1:
            level = "mid"
        else:
            level = "hard"
        user_tests_dict = dict()
        user_tests_dict['title'] = ut.userTitle
        user_tests_dict['group'] = ut.userGroup
        user_tests_dict['theme'] = ut.theme
        user_tests_dict['level'] = level
        user_tests_dict['start'] = ut.start
        user_tests_dict['end'] = ut.end
        time_list = str(ut.end - ut.start).split(":")
        user_tests_dict['time'] = time_list[1] + " " + str(int(float(time_list[2])))
        user_tests_dict['percent'] = ut.percent
        user_tests_dict['mark'] = ut.mark
        user_tests_list.append(user_tests_dict)

    return jsonify({"info": user_tests_list}), 200
