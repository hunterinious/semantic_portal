from app import db
from datetime import datetime


# class User(db.Model):
#     __tablename__ = 'users'
#     id = db.Column(db.Integer, nullable=False)
#     login = db.Column(db.VARCHAR(100), default=None)
#     psw = db.Column(db.VARCHAR(100), default=None)
#     name = db.Column(db.VARCHAR(255), default=None)
#     surname = db.Column(db.VARCHAR(255), default=None)
#     patronymic = db.Column(db.VARCHAR(255), default=None)
#     email = db.Column(db.VARCHAR(100), default=None)


class UserTest(db.Model):
    __tablename__ = 'user_testings'
    id = db.Column(db.Integer, nullable=False, primary_key=True)
    userTitle = db.Column(db.VARCHAR(250), nullable=False, default='')
    userGroup = db.Column(db.VARCHAR(250), nullable=False, default='')
    history = db.Column(db.VARCHAR(255), nullable=False)
    theme = db.Column(db.VARCHAR(250), nullable=False, default='')
    level = db.Column(db.SMALLINT, default=None)
    start = db.Column(db.DATETIME, default=None)
    end = db.Column(db.DATETIME, default=None)
    mark = db.Column(db.SMALLINT, default=None)
    percent = db.Column(db.SMALLINT, default=None)
    # correct_answers = db.Column(db.SMALLINT, default=0)
    # wrong_answers = db.Column(db.SMALLINT, default=0)

    def __init__(self, user_title: str, user_group: str, history: str):
        self.userTitle = user_title
        self.userGroup = user_group
        self.history = history

    @classmethod
    def create_user_test(cls, caption: str, complexity_level: int, username: str, group: str) -> 'UserTest':
        new_user_test = UserTest(username, group, "new history")
        date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        new_user_test.theme = caption
        new_user_test.level = complexity_level
        new_user_test.start = date
        db.session.add(new_user_test)
        db.session.commit()
        return new_user_test

    @classmethod
    def end_user_test(cls, test_id: int, result: dict):
        user_test = cls.query.get(test_id)
        date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        user_test.percent = result["percent"]
        user_test.mark = result["mark"]
        user_test.end = date
        db.session.commit()

    @classmethod
    def get_test_result(cls, result: dict, percents: float):
        result["percent"] = percents
        if 60 <= percents < 75:
            result["mark"] = 3
            result["verdict"] = "Satisfactorily"
        elif 75 <= percents < 95:
            result["mark"] = 4
            result["verdict"] = "Good"
        elif percents >= 95:
            result["mark"] = 5
            result["verdict"] = "Excellent"
        else:
            result["verdict"] = "Test failed"
            if 35 <= percents < 60:
                result["mark"] = 2
            elif 10 <= percents < 35:
                result["mark"] = 1
            else:
                result["mark"] = 0
