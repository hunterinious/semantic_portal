import os
from flask import Flask
from flask_session import Session
from .db import db


def create_app():
    app = Flask(__name__)
    app.secret_key = os.urandom(24)
    app.config.from_object(os.environ['APP_SETTINGS'])

    db.init_app(app)
    Session(app)
    with app.test_request_context():
        db.create_all()

    import app.test_mod.controllers as test_mod
    import app.user_mod.controllers as user_mod
    import app.view_mod.controllers as view_mod
    import app.cache_mod.controllers as cache_mod

    app.register_blueprint(test_mod.module)
    app.register_blueprint(user_mod.module)
    app.register_blueprint(view_mod.module)
    app.register_blueprint(cache_mod.module)

    return app
