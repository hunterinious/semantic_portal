from enum import Enum


class TaskType(Enum):
    chooseOne = "chooseOne"
    chooseSeveral = "chooseSeveral"
    writeAnswer = "writeAnswer"
    matchItems = "matchItems"
    gapFill = "gapFill"
    removeWrong = "removeWrong"
    listItems = "list-items"
    listSequence = "list-sequence"
