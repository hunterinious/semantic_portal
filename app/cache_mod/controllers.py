from flask import jsonify, Blueprint
from .cache import Cache

module = Blueprint('cache', __name__, url_prefix='/cache')


@module.route('/dump_view_with_classes', methods=['GET'])
def dump_view_with_classes():
    cache = Cache()
    cache.get_suitable_themes_for_classes()
    return jsonify("Dump successfully created"), 200


@module.route('/dump_concepts_with_views', methods=['GET'])
def dump_concepts_with_views():
    cache = Cache()
    cache.get_concepts_with_views()
    return jsonify("Dump successfully created"), 200


@module.route('/dump_templates_with_views', methods=['GET'])
def dump_templates_with_views():
    cache = Cache()
    cache.get_templates_with_suit_views()
    return jsonify("Dump successfully created"), 200


@module.route('/dump_main_with_aspects_and_vv', methods=['GET'])
def dump_main_with_aspects_and_vv():
    cache = Cache()
    cache.get_main_with_aspects_and_vv()
    return jsonify("Dump successfully created"), 200
