from app.test_mod.models import Concept, Thesis
from app.test_mod.template import Template
from collections import Counter
from app.db import db
import typing as t
import io
import json


class Cache:

    def get_suitable_themes_for_classes(self):
        # add to filter work classes
        concept = Concept()
        thesis = Thesis()
        concepts_cls_and_theses = db.session.query(Concept.clas, Thesis).join(
            Thesis, (Thesis.concept_id == Concept.id)).all()
        concepts_cls = concept.get_all_concepts_classes()
        theses_cls = thesis.get_all_theses_classes()

        concepts_cls = [c[0] for c in concepts_cls]
        theses_cls = [th[0] for th in theses_cls]

        suit_con_cls, suit_th_cls = self.build_classes_with_themes(concepts_cls_and_theses)

        for con_cls in concepts_cls:
            suit_con_cls[con_cls] = Counter(suit_con_cls[con_cls])
        for th_cls in theses_cls:
            suit_th_cls[th_cls] = Counter(suit_th_cls[th_cls])

        suit_cls = {key: {**suit_con_cls.get(key, {}), **suit_th_cls.get(key, {})} for key in (*suit_con_cls,
                                                                                               *suit_th_cls)}
        with io.open('classes_with_views.json', 'w', encoding='utf-8') as f:
            f.write(json.dumps(suit_cls, ensure_ascii=False))

    def build_classes_with_themes(self, concepts_cls_and_theses):
        suit_con_cls = dict()
        suit_th_cls = dict()

        for con_th in concepts_cls_and_theses:
            concept_clas = con_th[0]
            thesis = con_th[1]

            if suit_con_cls.get(concept_clas):
                suit_con_cls[concept_clas].append(thesis.view)
            else:
                suit_con_cls[concept_clas] = list()
                suit_con_cls[concept_clas].append(thesis.view)
            if suit_th_cls.get(thesis.clas):
                suit_th_cls[thesis.clas].append(thesis.view)
            else:
                suit_th_cls[thesis.clas] = list()
                suit_th_cls[thesis.clas].append(thesis.view)

        return suit_con_cls, suit_th_cls

    def get_concepts_with_views(self):
        con_ids_with_th_views = db.session.query(Concept.id, Thesis.view).join(
            Thesis, (Thesis.concept_id == Concept.id)).filter(Thesis.thesis != "", Concept.concept != "").all()

        concepts_with_views = dict()
        for id_view in con_ids_with_th_views:
            c_id = id_view[0]
            view = id_view[1]
            if not concepts_with_views.get(c_id):
                concepts_with_views[c_id] = list()
                concepts_with_views[c_id].append(view)
            elif view not in concepts_with_views[c_id]:
                concepts_with_views[c_id].append(view)

        with io.open('concepts_with_views.json', 'w', encoding='utf-8') as f:
            f.write(json.dumps(concepts_with_views, ensure_ascii=False))

    def get_templates_with_suit_views(self):
        template = Template()
        template.get_templates()
        templates = template.templates
        with io.open('classes_with_views.json') as f:
            classes_with_views = json.load(f)

        temp_with_views = list()
        for templ in templates:
            template_with_views = dict()
            template_with_views["id"] = templ["id"]
            template_with_views["name"] = templ["name"]
            template_with_views["questEntity"] = templ["questEntity"]
            template_with_views["questClass"] = templ["questClass"]
            template_with_views["questNotClass"] = templ["questNotClass"]
            template_with_views["question"] = templ["question"]
            template_with_views["variantClass"] = templ["variantClass"]
            template_with_views["variantNotClass"] = templ["variantNotClass"]
            template_with_views["types"] = templ["types"]

            self.build_classes_with_views_for_template(templ, template_with_views, "questClass",
                                                       "questClassViews", classes_with_views)
            self.build_classes_with_views_for_template(templ, template_with_views, "variantClass",
                                                       "variantClassViews", classes_with_views)

            temp_with_views.append(template_with_views)

        with io.open('templates_with_views.json', 'w', encoding='utf-8') as f:
            f.write(json.dumps(temp_with_views, ensure_ascii=False))

    def build_classes_with_views_for_template(self, templ, templ_with_views, cls, cls_views, cls_views_json):
        if len(templ[cls]) > 0:
            templ_with_views[cls_views] = dict()
            classes = templ[cls]
            for cl in classes:
                if cls_views_json.get(cl):
                    views = cls_views_json[cl].keys()
                    templ_with_views[cls_views][cl] = list()
                    for view in views:
                        templ_with_views[cls_views][cl].append(view)
        else:
            templ[cls_views] = dict()

    def get_main_with_aspects_and_vv(self):
        concepts = db.session.query(Concept).join(Thesis, (Thesis.concept_id == Concept.id)).filter(
            db.and_(Thesis.thesis != "", Concept.concept != ""))

        main_with_asp_and_vv = dict()

        for con in concepts:
            c_id = con.id
            if con.is_aspect:
                main_with_asp_and_vv[str(c_id) + "r"] = True
                main_id = db.session.query(Concept.aspect_of).filter(Concept.id == c_id)
                main_with_asp_and_vv[c_id] = list()
                mwa_and_vv = main_with_asp_and_vv[c_id]

                while main_id:
                    main_with_asp_and_vv[c_id].append(main_id.first()[0])
                    main_id = db.session.query(Concept.aspect_of).filter(Concept.is_aspect == 1,
                                                                         Concept.id == mwa_and_vv[len(mwa_and_vv) - 1])
                    if len(main_id.all()) == 0:
                        break
            else:
                main_with_asp_and_vv[str(c_id) + "r"] = False
                aspects_ids = db.session.query(Concept.id).filter(Concept.aspect_of == con.id)
                aspects_ids_list = list()

                while aspects_ids:
                    aspects_ids_list = aspects_ids_list + aspects_ids.all()
                    aspects_ids = db.session.query(Concept.id).filter(Concept.aspect_of in aspects_ids_list)

                    if len(aspects_ids.all()) == 0:
                        break

                main_with_asp_and_vv[c_id] = list()
                for asp in aspects_ids_list:
                    main_with_asp_and_vv[c_id].append(asp.id)
        db.session.close()

        with io.open('main_with_aspects_and_vv.json', 'w', encoding='utf-8') as f:
            f.write(json.dumps(main_with_asp_and_vv, ensure_ascii=False))



