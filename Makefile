setup-git-hooks:
	$(eval BASEDIR := $(shell git rev-parse --show-toplevel))
	$(eval REPO_HOOK := "$(BASEDIR)/.git/hooks/pre-commit")
	$(eval LOCAL_HOOK := "$(BASEDIR)/git-hooks/pre-commit")
	@echo "Creating symlink $(REPO_HOOK) to $(LOCAL_HOOK)"
	@ln -si $(LOCAL_HOOK) $(REPO_HOOK)


db:
	docker-compose up -d db


db-shell: db
	docker-compose run --rm db mysql -h db -uroot -psemantic_portal


db-load-dump: db
	docker-compose run --rm db sh -c 'mysql -h db -uroot -psemantic_portal semantic_portal < /opt/instance/db_dumps/dump.sql'


db-reset: db
	docker-compose run --rm db sh -c \
		'mysql -h db -uroot -psemantic_portal -e "DROP DATABASE IF EXISTS $$MYSQL_DB;" && \
		 mysql -h db -uroot -psemantic_portal -e "DROP DATABASE IF EXISTS $$MYSQL_TEST_DB;" && \
		 mysql -h db -uroot -psemantic_portal -e "CREATE DATABASE $$MYSQL_DB;" && \
		 mysql -h db -uroot -psemantic_portal -e "CREATE DATABASE $$MYSQL_TEST_DB;"'


run-dev: db
	cd src; FLASK_DEBUG=1 ./manage.py run -h 0.0.0.0 --with-threads
