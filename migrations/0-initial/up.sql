-- SQL ALTER statements for database migration
CREATE TABLE tests (
  id INTEGER NOT NULL AUTO_INCREMENT ,
  type VARCHAR(50) NOT NULL ,
  question TEXT NOT NULL ,
  template TEXT,
  variants JSON NOT NULL ,
  correct_answers JSON NOT NULL ,
  PRIMARY KEY (id)
);

CREATE TABLE tasks (
  id INTEGER NOT NULL AUTO_INCREMENT ,
  correct_answers INTEGER(11) NOT NULL ,
  wrong_answers INTEGER(11) NOT NULL ,
  percent INTEGER(11) NOT NULL ,
  mark INTEGER(11) NOT NULL ,
  verdict TEXT NOT NULL ,
  advice TEXT NOT NULL ,
  PRIMARY KEY (id)
);